/* globals describe, it */

const assert = require('assert')
const ethnologue = require('..')

describe('ethnologue', function () {
  it('is an array', function () {
    assert(Array.isArray(ethnologue))
    assert(ethnologue.length)
  })

  it('has over 7000 languages', function () {
    assert(ethnologue.length > 7000)
  })
})
