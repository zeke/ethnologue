const read = require('fs').readFileSync
const write = require('fs').writeFileSync
const exists = require('fs').existsSync
const dataDir = './Language_Code_Data_20150221'
const cheerio = require('cheerio')
const http = require('superagent')
const find = require('lodash').find
const RateLimiter = require('limiter').RateLimiter
var limiter = new RateLimiter(1, 'second')

// CountryCodes.tab
// The list of two-letter country codes that are used in the main language code table.

var countries = read(`${dataDir}/CountryCodes.tab`, 'utf8')
  .split('\r\n')
  .slice(1)
  .map(function (line) {
    var parts = line.split('\t')
    return {
      code: parts[0],
      name: parts[1],
      area: parts[2]
    }
  })
  .filter(function (country) {
    return country && country.code && country.name && country.area
  })

write('./countries.json', JSON.stringify(countries, null, 2))
console.log('wrote countries.json')

// LanguageCodes.tab
// The complete list of three-letter language identifiers used in the current
// Ethnologue (along with name, primary country, and language status).

var language_codes = read(`${dataDir}/LanguageCodes.tab`, 'utf8')
  .split('\r\n')
  .slice(1)
  .map(function (line) {
    var parts = line.split('\t')
    return {
      id: parts[0],
      name: parts[3],
      country: find(countries, 'code', parts[1]),
      status: parts[2]
    }
  })
  .filter(function (_) {
    return _ &&
      _.id &&
      _.country &&
      _.status &&
      _.name
  })// .slice(0,10)

write('./language_codes.json', JSON.stringify(language_codes, null, 2))
console.log('wrote language_codes.json')

var languages = []

language_codes
  .forEach(function (language) {
    var $
    var url = 'https://www.ethnologue.com/language/' + language.id
    var langFile = `./data/${language.id}.json`

    if (exists(langFile)) {
      console.log(`  ${language.id}.json already exists`)
      return
    } else {
      console.log(`  ${language.id} queued up`)
    }

    limiter.removeTokens(1, function () {
      http.get(url)
        .end(function (err, res) {
          if (err) return console.error(err)

          $ = cheerio.load(res.text)

          $('.field').each(function () {
            var key = $(this).find('.field-label').text()
              .replace(/\n/g, '')
              .trim()
              .toLowerCase()
              .replace(' ', '_')
              .replace('language_', '')
            var value = $(this).find('.field-items').text()
              .replace(/\n/g, '')
              .trim()
              .replace(/\.$/, '')

            if (key && value && !key.match('iso_639')) {
              language[key] = value
            }
          })

          console.log('  ' + language.name)
          languages.push(language)

          write(langFile, JSON.stringify(language, null, 2))

          if (language_codes.length === languages.length) {
            write('./languages.json', JSON.stringify(languages, null, 2))
            console.log('wrote languages.json')
          }
        })
    })
  })
