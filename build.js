var fs = require('fs')

var languages = fs.readdirSync('./data').map(function (file) {
  return JSON.parse(fs.readFileSync('./data/' + file, 'utf8'))
})

fs.writeFileSync('./languages.json', JSON.stringify(languages, null, 2))
